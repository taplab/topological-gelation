# Topological Gelation

MD simulations of reconnecting polymer rings with LAMMPS

Instructions:

1) Recplace fix_bond_swap.cpp in LAMMPS src folder and recompile LAMMPS
2) Run the simulation

- recomb_rings_in_sphere_fene.lam is the LAMMPS input file,
- nr1_lr1000_K2_R7.restart_56000000.dat in Init_confs is the starting configuration.
